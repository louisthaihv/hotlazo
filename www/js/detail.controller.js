appCtrl.controller('DetailController', function ($scope, $stateParams, $http, $ionicActionSheet, $cordovaSocialSharing) {
 	$scope.post_id = $stateParams.post_id;
 	$http.get('http://zolahot.com/ajaxpost/'+ $stateParams.post_id).
    success(function(data, status, headers, config) {
      $scope.post = data;
    }).error(function (data, status, headers, config) {
      alert("have an error!");
    });

    $scope.shareAnywhere = function() {
        $cordovaSocialSharing.share("This is your message", "This is your subject", "", "http://zolahot.com");
    }

    $scope.share = function(id, type){
    $ionicActionSheet.show({
      titleText: 'Select social',
      buttons: [
        { text: '<i class="ion-social-facebook"></i>  Facebook' },
        { text: '<i class="ion-social-twitter"></i>  Tweeter' },
        { text: '<i class="ion-social-googleplus"></i>  Google' },
      ],
      cancelText: 'Cancel',
      cancel: function () {
        console.log('CANCELLED');
      },
      buttonClicked: function(index) {
      	if(index === 0 ){
      		$scope.shareAnywhere();
      	}
      	else if(index === 1 ){
      		alert('tweeter');
      	}
       return true;
     }
    });
    }
})