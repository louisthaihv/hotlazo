appCtrl.controller('TopNewsController', function ($scope, $ionicSideMenuDelegate, HttpService){
    $scope.init = function() {
        $scope.posts = [];
        $scope.pageNumber = 1;
        $scope.canShowMore = true;
    };
    $scope.getResultsPage = function() {
        HttpService.requestAPI('get', 'ajax/post/top?page=' + $scope.pageNumber, {}, function(items){
            if (items.length === 0) {
                $scope.canShowMore = false;
            }
            else {
                $scope.pageNumber++;
                $scope.posts = $scope.posts.concat(items);
            }
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });
    };

    $scope.refresh = function() {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.init();
        $scope.getResultsPage();
    };
    
    $scope.search = function (){
        alert($scope.qSearch);
    };
    $scope.init();

})


