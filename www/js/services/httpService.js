appServices.service('HttpService', function($http, ConfigService) {
    this.requestAPI = function(method, url, data, next) {
        // Send request to server
        $http({
            method: method,
            url: ConfigService.baseUrl + url,
            data: data
        }).then(function(response) {
            next(response.data);
        });
    };
});